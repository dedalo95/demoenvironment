import { Component } from '@angular/core';
import * as RecordRTC from 'recordrtc';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import * as AWS from "aws-sdk";
@Component({
  selector: 'app-speech',
  templateUrl: './speech.component.html',
  styleUrls: ['./speech.component.scss']
})
export class SpeechComponent {
  token;
  results;
  service;
  s3;
  awsText
  private record;
  private recording = false;
  private url;
  private error;
  private webSocket;
  constructor(private domSanitizer: DomSanitizer,private http:HttpClient) {
    this.token="";
    this.service="";
    this.results="";
  }
  ngOnInit(): void {
    var credentials = new AWS.Credentials("AKIAJVCGCUOICHD4IM7A","kSQ3nscUyyeHBTi/ooaOt2ry9QuZrzi//pcKpEmp");
    AWS.config.credentials=credentials;
    this.s3 = new AWS.S3();
  }
  startSession(){
    let headers = new HttpHeaders();
    //Inizializzo header per get
    headers.append('Content-Type', "text");
    headers.append('projectid', "demo");
    this.http.get("http://localhost:8080/speech",{ headers: headers, responseType: 'text' }).subscribe((token)=>{
      this.token=token;
      var wsUrl='wss://stream.watsonplatform.net/speech-to-text/api/v1/recognize?access_token='+this.token+'&model=en-UK_BroadbandModel';
      this.webSocket=new WebSocket(wsUrl);
      this.webSocket.onopen = (event)=>{
        var message={
          "action":"start",
          "content-type":"audio/wav",
          "interim_results": true
        };
        this.webSocket.send(JSON.stringify(message));
        this.webSocket.onmessage=(evt)=>{
          console.log(evt.data);
          if(JSON.parse(evt.data).results!=undefined){
            this.results=JSON.parse(evt.data).results[0].alternatives[0].transcript;
          }
        };
        this.webSocket.onerror=(evt)=>{console.log(evt.data);this.webSocket.close()};
      };
      console.log(this.token);
    });
  }
  sanitize(url:string){
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }
  startRecording(){
    this.results="";
    if(this.webSocket!=undefined)
      this.webSocket.close();
    this.startSession();
    this.url="";
    this.recording = true;
    let mediaConstraints = {
        video: false,
        audio: true
    };
    navigator.mediaDevices
        .getUserMedia(mediaConstraints)
        .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }
  successCallback(stream) {
    var options = {
      type: 'audio/wav',
      recorderType: StereoAudioRecorder,
      desiredSampRate: 16000
    };
    var StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
  }

  stopRecording(){
    this.recording = false;
    this.record.stop(this.processRecording.bind(this));
  }
  processRecording(blob) {
    this.url = URL.createObjectURL(blob);
    this.webSocket.send(blob);
    blob.name="audio.wav";
    var params = {
      Bucket:"audiofilesbucket",
      Key:"audio.wav",
      Body:blob
    }
    this.s3.upload(params,(err,file)=>{
      if(err){
        console.log(err);
      }
      else{
        console.log(file)
        let headers = new HttpHeaders();
        //Inizializzo header per get
        headers.append('Content-Type', "text");
        headers.append('projectid', "demo");
        this.http.get("http://localhost:8080/aws",{ headers: headers, responseType: 'text' }).subscribe((response)=>{
          /*
          var ctrl=false;
          while(!ctrl){
            setTimeout(()=>{ 
              this.http.get("http://localhost:8080/check",{ headers: headers, responseType: 'text' }).subscribe((response)=>{
                if(JSON.parse(response).TranscriptionJobStatus=="COMPLETED"){
                  ctrl=true;
                  this.awsText=JSON.parse(response).TranscriptFileUri;
                  var params = {
                    Bucket: "audiofilesbucket", 
                    Key: "audio.wav"
                   };
                  this.s3.deleteObject(params,(err,data)=>{
                    if(err)
                      console.log(err);
                    else
                      console.log(data);
                  });
                }
              });
            }, 30000);
          }
          */
        });
      }
    });
  }
    errorCallback(error) {
    this.error = 'Can not play audio in your browser';
  }
}