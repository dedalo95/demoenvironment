import { Component, OnInit } from '@angular/core';
import { GoogleChartInterface } from 'ng2-google-charts/google-charts-interfaces';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public geoChart:GoogleChartInterface = {
    chartType:"GeoChart",
    dataTable:[
      ['Country', 'Popularity'],
      ['Germany', 300],
      ['United States', 700],
      ['Brazil', 400],
      ['Canada', 500],
      ['France', 600],
      ['Italy',600],
      ['Australia',400],
      ['United Kingdom',450],
      ['Ireland',350],
      ['RU', 500]
    ]
  }
  public treeChart:GoogleChartInterface = {
    chartType:"WordTree",
    dataTable:[['id', 'childLabel', 'parent', 'size', { role: 'style' }],
    [0, 'Life', -1, 1, 'blue'],
    [1, 'Archaea', 0, 1, 'green'],
    [2, 'Eukarya', 0, 5, 'green'],
    [3, 'Bacteria', 0, 1, 'green'],

    [4, 'Crenarchaeota', 1, 1, 'black'],
    [5, 'Euryarchaeota', 1, 1, 'black'],
    [6, 'Korarchaeota', 1, 1, 'black'],
    [7, 'Nanoarchaeota', 1, 1, 'black'],
    [8, 'Thaumarchaeota', 1, 1, 'black'],

    [9, 'Amoebae', 2, 1, 'black'],
    [10, 'Plants', 2, 1, 'black'],
    [11, 'Chromalveolata', 2, 1, 'black'],
    [12, 'Opisthokonta', 2, 5, 'black'],
    [13, 'Rhizaria', 2, 1, 'black'],
    [14, 'Excavata', 2, 1, 'black'],

    [15, 'Animalia', 12, 5, 'black'],
    [16, 'Fungi', 12, 2, 'black'],

    [17, 'Parazoa', 15, 2, 'black'],
    [18, 'Eumetazoa', 15, 5, 'black'],

    [19, 'Radiata', 18, 2, 'black'],
    [20, 'Bilateria', 18, 5, 'black'],

    [21, 'Orthonectida', 20, 2, 'black'],
    [22, 'Rhombozoa', 20, 2, 'black'],
    [23, 'Acoelomorpha', 20, 1, 'black'],
    [24, 'Deuterostomia', 20, 5, 'black'],
    [25, 'Chaetognatha', 20, 2, 'black'],
    [26, 'Protostomia', 20, 2, 'black'],

    [27, 'Chordata', 24, 5, 'black'],
    [28, 'Hemichordata', 24, 1, 'black'],
    [29, 'Echinodermata', 24, 1, 'black'],
    [30, 'Xenoturbellida', 24, 1, 'black'],
    [31, 'Vetulicolia', 24, 1, 'black']],
    options:{maxFontSize: 14, colors: ['black', 'black', 'black'],}
  }
  public barChart:GoogleChartInterface = {
    chartType:'BarChart',
    dataTable:[
      ['Most popular Frameworks','%'],
      ['Node JS',49.6],
      ['Angular',36.9],
      ['React',27.8],
      ['.NET',27.2],
      ['Spring',17.6],
      ['Django',13],
      ['Cordova',8.5],
      ['Tensorflow',7.8],
      ['Xamarin',7.4],
      ['Spark',4.8],
      ['Hadoop',4.7],
      ['PyTorch',1.7]
    ]
  }
  constructor() { }

  ngOnInit() {
  
  }
}
