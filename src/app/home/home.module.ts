import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { Routes,RouterModule } from '@angular/router';
import {HttpModule} from "@angular/http";
import {Http} from "@angular/http";
import { HttpClientModule } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { HomePage } from './home.page';
import { ShowPage } from '../show/show.page';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { SpeechComponent } from '../speech/speech.component';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    IonicModule,
    Ng2GoogleChartsModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
      {path:"dashboard",component:DashboardComponent},
      {path:"speech",component:SpeechComponent}
    ])
  ],
  declarations: [HomePage,DashboardComponent,SpeechComponent]
})
export class HomePageModule {}
