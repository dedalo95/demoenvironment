import { Component,ViewChild } from '@angular/core';
import {ElementRef} from "@angular/core";
import {HttpModule} from "@angular/http";
import {Http} from "@angular/http";
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  speech
  chat;
  search;
  dashboard;
  allmessages;
  newmessage;
  searchQuery;
  arrayResults;
  found;
  constructor(private http:HttpClient,public alertController: AlertController,private router: Router){
    this.chat=true;
    this.speech=false;
    this.search=false;
    this.dashboard=false;
    this.allmessages=[];
    this.newmessage="";
    this.searchQuery="";
    this.arrayResults=[];
    this.found=true;
  }
  ngOnInit() {
    this.router.navigate(['/home']);
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { }    
  }
  ngAfterViewChecked() {        
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { }    
  }
  showMore(item){
    this.router.navigate(["/show",item]);
  }
  addMessage(){
    console.log(this.newmessage);
    if(this.newmessage!=""&&this.newmessage!=" "&&this.newmessage!="  "){
      this.allmessages.push({
        type:true,
        text:this.newmessage
      });
      let headers = new HttpHeaders();
      //Inizializzo header per get
      headers.append('Content-Type', "text");
      headers.append('projectid', "ChatApp");
      //Inizializzo parametri get
      let params = new HttpParams().set("data",this.newmessage);
        this.http.get("http://localhost:8080/chat",{ headers: headers, params: params, responseType: 'text' }).subscribe(
          (response)=>{
            console.log(response);
            var jsonRes=JSON.parse(response);
            console.log(jsonRes);
            for(var i in jsonRes.output.text){
              this.allmessages.push({
                type:false,
                text:jsonRes.output.text[i]
              });
            }
          }
        );
        this.newmessage="";
      }
  }
  searchData(){
    this.found=false;
    this.arrayResults=[];
    let headers = new HttpHeaders();
    //Inizializzo header per get
    headers.append('Content-Type', "text");
    headers.append('projectid', "ChatApp");
    let params = new HttpParams().set("data",this.searchQuery);
    this.http.get("http://localhost:8080/search",{ headers: headers, params: params, responseType: 'text' }).subscribe(
      (response)=>{
        var jsonRes=JSON.parse(response);
        console.log(jsonRes);
        for(var i in jsonRes){
          this.arrayResults.push({
            title:jsonRes[i].title,
            text:jsonRes[i].text
          });
          this.found=true;
        }
      }
    );
    this.searchQuery="";
  }
  goChat(){
    this.chat=true;
    this.search=false;
    this.dashboard=false;
    this.speech=false;
  }
  goSearch(){
    this.chat=false;
    this.search=true;
    this.dashboard=false;
    this.speech=false;
  }
  goSpeech(){
    this.chat=false;
    this.search=false;
    this.dashboard=false;
    this.speech=true;
  }
  goDashboard(){
    this.chat=false;
    this.search=false;
    this.dashboard=true;
    this.speech=false;
  }
}
