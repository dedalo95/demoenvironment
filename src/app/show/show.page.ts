import { Component, OnInit } from '@angular/core';
import {NavController} from "@ionic/angular";
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-show',
  templateUrl: './show.page.html',
  styleUrls: ['./show.page.scss'],
})
export class ShowPage implements OnInit {
  content
  constructor(public route:ActivatedRoute) { 
  }

  ngOnInit() {
    this.route.params.subscribe(data => {
      this.content = data;
      console.log(this.content);
      
   })
  }

}
